package com.platzi.course.domain;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Product {

    private long productId;
    private String name;
    private int categoryId;
    private double price;
    private int stock;
    private boolean active;
    private Category category;

}

